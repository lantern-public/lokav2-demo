/**
 * @file WiSOL.h
 * @author Andres Arias
 * @date 24/9/2019
 * @brief Driver for the WiSOL Sigfox modem.
 */

#ifndef WISOL_H
#define WISOL_H

#include <string>
#include <cstring>
#include <cstdio>
#include "esp_log.h"
#include "LOKA_UART.h"
#include "LOKA_DigitalPin.h"
#include "BoardIO.h"

/**
 * @brief Tag used by the logging library.
 */
#define WISOL_TAG           "WiSOL"

/**
 * @brief Period for timing out simple modem requests.
 */
#define WISOL_TIMEOUT       250
/**
 * @brief Period for timing out uplink transmission requests.
 */
#define WISOL_TX_TIMEOUT    9000
/**
 * @brief Period for timing out downlink transmission/reception requests.
 */
#define WISOL_RX_TIMEOUT    60000
/**
 * @brief Size of the buffer where the modem will store the output data.
 */
#define WISOL_BUFF_SIZE     1024

namespace drivers {

    /**
     * @enum SigfoxZone
     * @brief Indicates the Sigfox zone assigned for the current device.
     */
    enum class SigfoxZone {
        /** Zone 1: Europe and overseas France. */
        RCZ1,
        /** Zone 2: Brazil, Canada, Mexico, Puerto Rico and USA. */
        RCZ2,
        /** Zone 3: Japan. */
        RCZ3,
        /** Zone 4: Latin America, Caribbean and Pacific Asia. */
        RCZ4,
        /** Zone 5: South Korea. */
        RCZ5,
        /** Zone 6: India. */
        RCZ6
    };

    /**
     * @class WiSOL
     * @brief Low-level driver for the WiSOL Sigfox modem.
     */
    class WiSOL {
        public:

            /**
             * @brief Class constructor. Initializes the module.
             */
            WiSOL(void);

            /**
             * @brief Class destructor. If the module was awake, sends it
             * to sleep and then deinitializes the module.
             */
            ~WiSOL(void);

            /**
             * @brief Wakes the module from deep sleep.
             * @retval ESP_OK Module is up and running normally.
             * @retval ESP_FAIL Module did not answer.
             */
            esp_err_t turnOn(void);

            /**
             * @brief Makes the module go to sleep.
             * @retval ESP_OK Module is in deep sleep.
             * @retval ESP_FAIL Module failed to go to sleep.
             */
            esp_err_t turnOff(void);

            /**
             * @brief Indicates whether the module is awake or not.
             * @retval true Module is up and running.
             * @retval false Module is in deep sleep mode.
             */
            bool isOn(void);

            /**
             * @brief Retrieves the Sigfox ID from the modem
             * @return String containing the ID in hex form.
             */
            std::string getID(void);

            /**
             * @brief Retrieves the initial Sigfox PAC from the modem
             * @return String containing the PAC in hex form.
             */
            std::string getPAC(void);

            /**
             * @brief Sends the string in hex ASCII as an uplink message.
             * @param[in] data The data to send as a message.
             * @retval ESP_OK Modem sent the message successfully.
             * @retval ESP_FAIL Modem failed to send the message.
             */
            esp_err_t sendData(const std::string &data);

            /**
             * @brief Sends the string in hex ASCII as an uplink message, but
             * then requests a downlink message.
             * @param[in] data The data to send as a message.
             * @return String containing the data retrieved from the downlink
             * message. NOTE: Empty string means no available downlink.
             */
            std::string sendAndReadData(const std::string &input);

            /**
             * @brief Starts the Continuous Wave test mode, where the device
             * will send a signal using the RCZ designated frequency without 
             * any modulation.
             * @note This function is only required for the Sigfox emission tests
             * performed during the certification process.
             * @retval ESP_OK Device is not on CW mode.
             * @retval ESP_FAIL Device failed to enter CW mode.
             */
            esp_err_t startCWMode(SigfoxZone zone);

            /**
             * @brief Stops the Continuous Wave test mode.
             * @retval ESP_OK Device is now operating normally.
             * @retval ESP_FAIL Device failed to exit CW Mode.
             */
            esp_err_t stopCWMode(void);

        private:
            UART uart_ctrl; /** The UART bus driver used to talk to the modem. */
            bool is_on; /** Indicates whether the modem is awake or not. */
            bool is_fcc; /** Indicates if the modem is FCC compliant or ETSI compliant. */
            
            /**
             * @brief Reads the UART bus and then cleans the buffer, removing 
             * any newlines added to the output.
             * @note The WISOL modules add \r\n to the end the data stream.
             * @return String containing the read data after clean up.
             */
            std::string readClean(unsigned int timeout);

            /**
             * @brief Builds a string containing the appropriate AT command
             * for uplink and downlink messages.
             * @note Must manually add the \r\n for uplinks or ,1\r\n for 
             * downlinks to the resulting string after this function is called.
             * @param[out] buffer The resulting AT command.
             * @param[in] size The size of the AT command buffer.
             * @param[in] data The string containing the data to be parsed.
             */
            void buildATCommand(char *buffer,
                                unsigned int size,
                                const std::string &data) const;

            /**
             * @brief Parses the received downlink as reported by the modem.
             * @param[in] raw_data The string containing the data as read 
             * from the modem.
             * @return String containing the parsed data.
             */
            std::string parseDownlink(const std::string &raw_data);

    }; 
} /* drivers */ 

#endif
